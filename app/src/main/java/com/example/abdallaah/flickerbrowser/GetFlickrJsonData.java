package com.example.abdallaah.flickerbrowser;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

//The string here is the same parameter as executeOnSameThread string, we are not doing progress bar so void and returning Photo
class GetFlickrJsonData extends AsyncTask<String, Void, List<Photo>> implements GetRawData.OnDownloadComplete {
    private static final String TAG = "GetFlickrJsonData";

    //Photobjexcts that we are gonna store

    private List<Photo> mPhotoList = null;
    private String mBaseURL;
    private String mLanguage;
    private boolean mMatchAll;

    //Store the callback Object
    private final OnDataAvailable mCallBack;
    private boolean runningOnSameThrad = false;

    GetFlickrJsonData( String baseURL, String language, boolean matchAll, OnDataAvailable callBack) {
        Log.d(TAG, "GetFlickrJsonData: called");
        this.mBaseURL = baseURL;
        this.mLanguage = language;
        this.mMatchAll = matchAll;
        this.mCallBack = callBack;
        Log.d(TAG, "GetFlickrJsonData: finished Intialization");
        //onDownloadComplete has mPhotolist
    }

    //Give MainActivity the Callback that data is available begin parsing
    interface OnDataAvailable {
        void onDataAvailable(List<Photo> data, DownloadStatus status);
    }

    void executeOnSameThread(String searchCriteria){
        Log.d(TAG, "executeOnSameThread: starts");
        runningOnSameThrad = true;
        String destinationUri = createUri(searchCriteria, mLanguage, mMatchAll);

        GetRawData getRawData = new GetRawData(this);
        getRawData.execute(destinationUri);
        Log.d(TAG, "executeOnSameThread: ends");
    }

    @Override
    protected void onPostExecute(List<Photo> photos) {
        Log.d(TAG, "onPostExecute: starts");
        if(mCallBack != null){
            mCallBack.onDataAvailable(mPhotoList, DownloadStatus.OK);
        }
        Log.d(TAG, "onPostExecute: ends");
    }

    //It will do the exact same thing as what executeOnSameThread does but on a backgroudn thread
    @Override
    protected List<Photo> doInBackground(String... params) {
        Log.d(TAG, "doInBackground: starts");

        String destinationUri = createUri(params[0], mLanguage, mMatchAll);
        GetRawData getRawData = new GetRawData(this);
        getRawData.runInSameThread(destinationUri);
        Log.d(TAG, "doInBackground: ends");
        return mPhotoList;
    }

    private String createUri(String searchCriteria, String lang, boolean matchAll){
        Log.d(TAG, "createUri: starts");
        String uri = Uri.parse(mBaseURL).buildUpon()
        .appendQueryParameter("tags", searchCriteria)
        .appendQueryParameter("tagMode", matchAll ? "ALL" : "ANY")
        .appendQueryParameter("lang", lang)
        .appendQueryParameter("format", "json")
        .appendQueryParameter("nojsoncallback", "1")
        .build().toString();
        Log.e(TAG, "createUri: returned this url = "+  uri);
        return  uri;

    }

    @Override
    public void onDownloadComplete(String data, DownloadStatus status) {
        Log.d(TAG, "onDownloadComplete: starts");
        Log.d(TAG, "onDownloadComplete: Status =" + status);
        if(status == DownloadStatus.OK){ //Then we obviously have some data to work with
           mPhotoList = new ArrayList<>();
            //We just have some data, Now we gotta check if its valid JSON
            try {
                JSONObject jsonData = new JSONObject(data);
                JSONArray itemArray = jsonData.getJSONArray("items");

                for(int i = 0; i<itemArray.length(); i++){ //itemArray=[item, item], item ={'title, 'author', 'photo'.. etc}
                    JSONObject jsonPhoto = itemArray.getJSONObject(i);
                    String title = jsonPhoto.getString("title");
                    String author = jsonPhoto.getString("author");
                    String authorId = jsonPhoto.getString("author_id");
                    String tags = jsonPhoto.getString("tags");
                    //Photo link is a nested item

                    JSONObject jsonMedia = jsonPhoto.getJSONObject("media");
                    String photoUrl = jsonMedia.getString("m");

                    //_m. is the version of image.. which is too big, _b. is smaller
                    String link = photoUrl.replaceFirst("_m.", "_b.");

                    Photo photoObject = new Photo(title, author, authorId, link, tags, photoUrl);
                    mPhotoList.add(photoObject);

                    Log.d(TAG, "onDownloadComplete: " + photoObject.toString());
                }
            }
            catch (JSONException exception){
                exception.printStackTrace();
                Log.e(TAG, "onDownloadComplete: Error in JSON " + exception.getMessage());
                //If there is an exception, the data can't be valid
                status = DownloadStatus.FAILED_OR_EMPTY;
            }
            if(runningOnSameThrad && mCallBack != null){
                //tell the caller the processing is done - return null if error happened
                mCallBack.onDataAvailable(mPhotoList, status);
            }

            Log.d(TAG, "onDownloadComplete: ends");
            //nwo inform the caller if mPhotolist is empty
        }
    }
}
