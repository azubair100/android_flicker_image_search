package com.example.abdallaah.flickerbrowser;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

enum DownloadStatus {IDLE, PROCESSING, NOT_INITIALIZED, FAILED_OR_EMPTY, OK}
//IDLE = NOT PROCESSING ANYTHING
// PROCESSING = DOWNLOADING JSON
//NOT_INITIALIZED =  DIDN'T WORK
//FAILED_OR_EMPTY = RUNTIME FAIL
//OK IS SUCCESSFUL


//THIS CLASS INITIATES THE DOWNLOAD SEQUENCE
//ONLY A MainActivity CLASS INSTANCE CONTAINING OnDownloadComplete INTERFACE CAN HAVE THE CALL BACK
class GetRawData extends AsyncTask<String, Void, String> {
    private static final String TAG = "GetRawData";

    //field to check download status
    private DownloadStatus mDownloadStatus;
    
    //WE ARE PASSING THE LISTENER INSIDE THE CLASS
    //CAUSE views HAVE setOnClickListeners already set up like new OnClick LIKE THE CLASS
    private final OnDownloadComplete mCallBack;

    //interface MEANS YOU HAVE TO IMPLEMENT THIS METHOD
    interface OnDownloadComplete{
        void onDownloadComplete(String data, DownloadStatus status);
    }


    public GetRawData(OnDownloadComplete callBack){
        Log.d(TAG, "GetRawData: callback started");
        this.mDownloadStatus = DownloadStatus.IDLE;
        mCallBack = callBack;
        Log.d(TAG, "GetRawData: ended");
    }


    @Override
    protected String doInBackground(String... strings) {
        HttpURLConnection connection = null;
        BufferedReader reader = null;
        Log.d(TAG, "doInBackground: started ");

        if(strings == null){
            mDownloadStatus = DownloadStatus.NOT_INITIALIZED;

            Log.d(TAG, "doInBackground: strings parameter was null");
            return null;
        }

        try{

            mDownloadStatus = DownloadStatus.PROCESSING;

            URL url =  new URL(strings[0]);
            //NOT HttpsURL
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();

            int response = connection.getResponseCode();
            Log.d(TAG, "doInBackground: The response was " + response);

            StringBuilder result = new StringBuilder();

            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            while(null != (line = reader.readLine())){
                result.append(line).append("\n");
            }
            mDownloadStatus = DownloadStatus.OK;

            return result.toString();
        }

        catch (MalformedURLException exception){
            Log.e(TAG, "doInBackground: Invalid URL :" + exception.getMessage() );
        }

        catch (IOException exception){
            Log.e(TAG, "doInBackground: IOException reading data: " + exception.getClass());
        }

        catch (SecurityException exception){
            Log.e(TAG, "doInBackground: Security Exception needs permission: " + exception.getMessage() );
        }

        finally {
            if(connection != null){
                connection.disconnect();
            }
            if(reader !=null){
                try{
                    reader.close();
                }
                catch (IOException exception){
                    Log.e(TAG, "doInBackground: Error Closing stream" + exception.getMessage());
                }
            }
        }

        mDownloadStatus = DownloadStatus.FAILED_OR_EMPTY;
        Log.d(TAG, "doInBackground: result was empty or failed");
        return null;
    }

    //
    void runInSameThread(String string){
        Log.d(TAG, "runInSameThread: starts");

        //onPostExecute(doInBackground(string));

        if(mCallBack != null){
            mCallBack.onDownloadComplete(doInBackground(string), mDownloadStatus);
        }

        Log.d(TAG, "runInSameThread: ends");
    }

    @Override
    protected void onPostExecute(String s) {
        Log.d(TAG, "onPostExecute: started");
        if(mCallBack != null){
            mCallBack.onDownloadComplete(s, mDownloadStatus);
        }
        Log.d(TAG, "onPostExecute: ends");
//        super.onPostExecute(s);
    }

}
