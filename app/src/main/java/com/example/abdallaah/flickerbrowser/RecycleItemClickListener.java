package com.example.abdallaah.flickerbrowser;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

class RecycleItemClickListener extends RecyclerView.SimpleOnItemTouchListener {
    private static final String TAG = "RecycleItemClickListene";

    interface OnRecyclerClickListener {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

    private final OnRecyclerClickListener mListener;
    private final GestureDetectorCompat mGestureDetector;

    //NEed Context context for gesture detect
    //Need RecyclerView recyclerView intercept all the touch event, any sort of touch happens

    public RecycleItemClickListener(Context context, final RecyclerView recyclerView, final OnRecyclerClickListener mListener) {
        this.mListener = mListener;
        mGestureDetector = new GestureDetectorCompat(context, new GestureDetector.SimpleOnGestureListener(){
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                Log.d(TAG, "onSingleTapUp: starts");
//                return super.onSingleTapUp(e);
                View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if(childView != null && mListener != null){
                    Log.d(TAG, "onSingleTapUp: calling Listener.onSingleTapUp");
                    mListener.onItemClick(childView, recyclerView.getChildAdapterPosition(childView));
                }
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                Log.d(TAG, "onLongPress: started");
                View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if(childView != null && mListener != null){
                    Log.d(TAG, "onSingleTapUp: calling Listener.onLongPress");
                    mListener.onItemLongClick(childView, recyclerView.getChildAdapterPosition(childView));
                }
//                super.onLongPress(e);
            }
        });
        
        
        
    }
    

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        Log.d(TAG, "onInterceptTouchEvent: started");
        //since we want th gesture detector to detect gesture
        //return super.onInterceptTouchEvent(rv, e);
        if (mGestureDetector != null) {
            boolean result = mGestureDetector.onTouchEvent(e);
            Log.d(TAG, "onInterceptTouchEvent: returned " + result);
            return result;
        } else {
            Log.d(TAG, "onInterceptTouchEvent: returned false");
            return false;
        }

    }
}

